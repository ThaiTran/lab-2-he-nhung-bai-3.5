/////////////////////////////////////////////////////////////////////////////////////
//                     Embedded Systems - IT4210 (SoICT-HUST)                      //
//						       Sample codes for Lab #2 							   //
/////////////////////////////////////////////////////////////////////////////////////
#include "reg51.h"
#include "string.h"
//Dinh nghia mot so chan de dieu khien den LCD
#define LCD_DATA P2
sbit EN=P0^2;
sbit RS=P0^0;
sbit RW=P0^1;	   
sbit SPK  = P3^7;	

//verify this table according to your CPU clock
code unsigned int note_table[]={
	// Base notes
	0xF9,0x1F,        // C4
	0xf9,0xDF,        // D4
	0xFA,0x8A,        // E4
	0xfa,0xD9,        // F4
	0xfb,0x68,        // G4
	0xfb,0xe9,        // A4
	0xfc,0x5b,        // B4
	0xfc,0x8f,        // C5
	0xfc,0xef,        // D5
	0xfd,0x45,        // E5
	0xfd,0x6c,        // F5
	0xfd,0xb4,        // G5
	0xfd,0xf4,        // A5
	0xfe,0x2d,        // B5
	0xfe,0x48,        // C6

	// Choi mot doan nhac trong bai Be len ba
	// Nốt chữ cái giọng C trưởng

	// Cháu lên ba, cháu đi mẫu giáo
	// E5 D5 C5, D5 C5 D5 E5
	// Cô thương cháu vì cháu không khóc nhè
	// D5 D5 E5 C5 E5 D5 G5 C5
	// Không khóc nhè để mẹ lên nương rẫy
	// D5 E5 C5 G4 G4 C5 D5 E5
	// Cha vào nhà máy, ông bà vui cấy cầy
	// D5 C5 C5 E5, D5 C5 D5 G5 C5
	// Là lá la là, là là là lá la là …
	// G4 E5 D5 C5, G4 G4 G4 E5 D5 C5…

	0xfd,0x45,        // E5
	0xfc,0xef,        // D5
	0xfc,0x8f,        // C5
	0xfc,0xef,        // D5
	0xfc,0x8f,        // C5
	0xfc,0xef,        // D5
	0xfd,0x45,        // E5

	0xfc,0xef,        // D5
	0xfc,0xef,        // D5
	0xfd,0x45,        // E5
	0xfc,0x8f,        // C5
	0xfd,0x45,        // E5
	0xfc,0xef,        // D5
	0xfd,0xb4,        // G5
	0xfc,0x8f,        // C5

	0xfc,0xef,        // D5
	0xfd,0x45,        // E5
	0xfc,0x8f,        // C5
	0xfb,0x68,        // G4
	0xfb,0x68,        // G4
	0xfc,0x8f,        // C5
	0xfc,0xef,        // D5
	0xfd,0x45,        // E5

	0xfc,0xef,        // D5
	0xfc,0x8f,        // C5
	0xfc,0x8f,        // C5
	0xfd,0x45,        // E5
	0xfc,0xef,        // D5
	0xfc,0x8f,        // C5
	0xfc,0xef,        // D5
	0xfd,0xb4,        // G5
	0xfc,0x8f,        // C5

	0xfb,0x68,        // G4
	0xfd,0x45,        // E5
	0xfc,0x8f,        // C5
	0xfc,0xef,        // D5
	0xfb,0x68,        // G4
	0xfb,0x68,        // G4
	0xfb,0x68,        // G4
	0xfd,0x45,        // E5
	0xfc,0xef,        // D5
	0xfc,0x8f,        // C5
};
unsigned char note_index;
//Khai bao prototype cho cac ham
void Init_System();	
void Delay_ms(int interval);  
void LCD_init();
void Wait_For_LCD();
void LCD_Send_Command(unsigned char x);
void LCD_Write_One_Char(unsigned char c);
void LCD_Write_String(unsigned char *s);
void Play_Music_Be_Len_Ba();
void Play_Note(unsigned char *s, int note);
void init();		
char uart_data;
char prev_data;

void main()
{
	Play_Music_Be_Len_Ba();
	SCON = 0x50; 			/* uart in mode 1 (8 bit), REN=1 */
	TMOD = TMOD | 0x20 ; 	/* Timer 1 in mode 2 */
	TH1 = 0xFD; 			/* 9600 Bds at 11.0592MHz */
	TL1 = 0xFD; 			/* 9600 Bds at 11.0592MHz */
	ES = 1; 				/* Enable serial interrupt*/
	EA = 1; 				/* Enable global interrupt */
	TR1 = 1; 				/* Timer 1 run */
	Init_System();
	LCD_init();

// Table convert
// Key:	Note
// q:	C4
// w:	D4
// e:	E4
// r:	F4
// t:	G4
// y:	A4
// u:	B4
// i:	C5
// o:	D5
// p:	E5
// a:	F5
// s:	G5
// d:	A5
// f:	B5
// g:	C6

	uart_data = 'h';
	while(1){		
		if (uart_data != 'h') {
			LCD_Send_Command(0x01); //Xoa man hinh
			switch (uart_data) {
				case 'q':
					Play_Note("C4", 0);
					break;
				case 'w':
					Play_Note("D4", 2);
					break;
				case 'e':
					Play_Note("E4", 4);
					break;
				case 'r':
					Play_Note("F4", 6);
					break;
				case 't':
					Play_Note("G4", 8);
					break;
				case 'y':
					Play_Note("A4", 10);
					break;
				case 'u':
					Play_Note("B4", 12);
					break;
				case 'i':
					Play_Note("C5", 14);
					break;
				case 'o':
					Play_Note("D5", 16);
					break;
				case 'p':
					Play_Note("E5", 18);
					break;
				case 'a':
					Play_Note("F5", 20);
					break;
				case 's':
					Play_Note("G5", 22);
					break;
				case 'd':
					Play_Note("A5", 24);
					break;
				case 'f':
					Play_Note("B5", 26);
					break;
				case 'g':
					Play_Note("C6", 28);
					break;
			}
			uart_data = 'h';
		}
	}
}

void init()
{
	TMOD=0x01;
	EA=1;
	ET0=1;
	TR0=0;
}

void timer0() interrupt 1
{
	TH0=note_table[note_index];
	TL0=note_table[note_index+1];
	SPK=~SPK;
}

void serial_IT(void) interrupt 4
{
  if (RI == 1)
  { 
	RI = 0; 			/* prepare for next reception */
	if (SBUF != prev_data) {
		uart_data = SBUF; 	/* Read receive data */
		prev_data = SBUF;
		// SBUF = uart_data; 	/* Send back same data on uart*/
	} else {
		uart_data = SBUF; 	/* Read receive data */
	}
  }
  else 
    TI = 0; 			/* if emission occur */
}

void Init_System()
{
	//Thiet lap LCD o che do doc
	RW=1;	
}
void Delay_ms(int interval)
{
	int i,j;
	for(i=0;i<1000;i++)
	{
		for(j=0;j<interval;j++);
	}
}
//Ham thuc hien gui mot lenh xuong LCD
void LCD_Send_Command(unsigned char x)
{
	LCD_DATA=x;
	RS=0; //Chon thanh ghi lenh
	RW=0; //De ghi du lieu
	EN=1;
	Delay_ms(1);
	EN=0;		
	Wait_For_LCD(); //Doi cho LCD san sang
	EN=1;		  
}
//Ham kiem tra va cho den khi LCD san sang
void Wait_For_LCD()
{
	Delay_ms(10);
}
void LCD_init()
{
	LCD_Send_Command(0x38); //Chon che do 8 bit, 2 hang cho LCD
	LCD_Send_Command(0x0E); //Bat hien thi, nhap nhay con tro	
	LCD_Send_Command(0x01); //Xoa man hinh	
	LCD_Send_Command(0x80); //Ve dau dong
	
}
//Ham de LCD hien thi mot ky tu
void LCD_Write_One_Char(unsigned char c)
{
	LCD_DATA=c; //Dua du lieu vao thanh ghi 
	RS=1; //Chon thanh ghi du lieu
	RW=0;
	EN=1;
	Delay_ms(1);	
	EN=0;
	Wait_For_LCD();	
	EN=1;
}
//Ham de LCD hien thi mot xau
void LCD_Write_String(unsigned char *s)
{
	unsigned char length;
	length=strlen(s); //Lay do dai xau
	while(length!=0)
	{
		LCD_Write_One_Char(*s); //Ghi ra LCD gia tri duoc tro boi con tro
		s++; //Tang con tro
		length--;	  		 
	}
}

void Play_Music_Be_Len_Ba()
{
	unsigned char i;
	init();
	note_index=30;
	for(i=0;i<42;i++){		
		TH0=note_table[note_index];
		TL0=note_table[note_index+1];			
		TR0=1;
		Delay_ms(10);
		TR0=0;
		SPK=1;
		Delay_ms(20);
		note_index+=2;			
	}
}

void Play_Note(unsigned char *s, int note)
{
	LCD_Write_String(s);
	note_index=note;
	TH0=note_table[note_index];
	TL0=note_table[note_index+1];			
	TR0=1;
	Delay_ms(10);
	TR0=0;
	SPK=1;
}


