import serial
import msvcrt

# khoi tao serial
s = serial.Serial()
s.port = "COM2"
s.baudrate = 9600
s.close()
s.open()

# Table convert
# Key:	Note
# q:	C4
# w:	D4
# e:	E4
# r:	F4
# t:	G4
# y:	A4
# u:	B4
# i:	C5
# o:	D5
# p:	E5
# a:	F5
# s:	G5
# d:	A5
# f:	B5
# g:	C6

while True:
	if msvcrt.kbhit(): # Kiem tra xem co bam phim khong
		note = msvcrt.getch() # lay gia tri phim vua nhan
		note = note.decode("ASCII") # chuyen sang ma ASCII
		s.write(bytes(note, "utf-8")) # gui byte ky tu qua cong COM2
		print(bytes(note, "utf-8")) # in ra ky tu tren man hinh
		if(note == "S"): 
			print("See you again!") # in ra cau thong bao See you again!
			break # thoat chuong trinh

# dong serial
s.close()